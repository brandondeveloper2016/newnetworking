﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UsersNetworking.Startup))]
namespace UsersNetworking
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
